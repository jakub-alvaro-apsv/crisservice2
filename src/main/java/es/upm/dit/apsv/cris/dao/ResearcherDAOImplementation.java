package es.upm.dit.apsv.cris.dao;

import java.util.List;
import javax.persistence.Entity;
import org.hibernate.Session;
import es.upm.dit.apsv.cris.model.Researcher;

public class ResearcherDAOImplementation implements ResearcherDAO {
	
	private static ResearcherDAOImplementation instance = null;
	
	private ResearcherDAOImplementation() {}
	
	//truco para que solo haya una instancia de este objeto en este programa
	public static ResearcherDAOImplementation getInstance() {
		if(null==instance) //si aun no se ha creado, se crea la primera vez
			instance = new ResearcherDAOImplementation();
		return instance;
	}

	@Override
	public Researcher create(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.save(researcher);
			session.getTransaction().commit(); //todo ha ido bien
		}catch(Exception e){
		} finally {
			session.close(); //por si se produce un error en la sesion con la bbdd, la cerramos al recoger el error
		}
		return researcher;
	}

	@Override
	public Researcher read(String researcherId) {
		Session session = SessionFactoryService.get().openSession();
		Researcher researcher = null;
		try {
			session.beginTransaction();
			researcher = session.get(Researcher.class, researcherId);
			session.getTransaction().commit(); //todo ha ido bien
		}catch(Exception e){
		} finally {
			session.close(); //por si se produce un error en la sesion con la bbdd, la cerramos al recoger el error
		}
		return researcher;
	
	}

	@Override
	public Researcher update(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(researcher);
			session.getTransaction().commit(); //todo ha ido bien
		}catch(Exception e){
		} finally {
			session.close(); //por si se produce un error en la sesion con la bbdd, la cerramos al recoger el error
		}
		return researcher;
	}

	@Override
	public Researcher delete(Researcher researcher) {
		
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.delete(researcher);
			session.getTransaction().commit(); //todo ha ido bien
		}catch(Exception e){
		} finally {
			session.close(); //por si se produce un error en la sesion con la bbdd, la cerramos al recoger el error
		}
		return researcher;
	}

	@Override
	public List<Researcher> readAll() {
		Session session = SessionFactoryService.get().openSession();
		List<Researcher> list = null;
		try {
			session.beginTransaction();
			list = (List<Researcher>) session.createQuery("from Researcher").getResultList();
			session.getTransaction().commit(); //todo ha ido bien
		}catch(Exception e){
		} finally {
			session.close(); 
		}
		return list;
	}

	@Override
	public Researcher readByEmail(String email) {
		//podemos hacerla haciendo una consulta SQL o filtrando la lista de readAll()
		for (Researcher researcher: this.readAll())
			if (email.equals(researcher.getEmail()))
				return researcher;
		return null;
	}

}
