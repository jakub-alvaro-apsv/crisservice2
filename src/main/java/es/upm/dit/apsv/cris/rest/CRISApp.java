package es.upm.dit.apsv.cris.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("rest")//forma parte de la ruta de entrada de las peticiones REST
public class CRISApp extends ResourceConfig {
	public CRISApp() {
		packages("es.upm.dit.apsv.cris.rest");
	}
}
