package es.upm.dit.apsv.cris.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

@Path("/Researchers") //a todos los metodos de la clase se llega poniendo /Researchers en la ruta
public class ResearcherResource {

	@GET //cuando llega un get a secas, se ejecuta este metodo
	@Produces(MediaType.APPLICATION_JSON)
	public List<Researcher> readAll(){
		return ResearcherDAOImplementation.getInstance().readAll();
		//hace falta poner el getInstance() porque solo puede haber un unico objeto de la clase
	}
	
	@POST //cuando llega POST en /researchers se crea un researcher
	@Consumes(MediaType.APPLICATION_JSON) // @Consumes -> el researcher llega en el cuerpo de la peticion, NO en la ruta
	public Response create (Researcher rnew) throws URISyntaxException {
		Researcher r = ResearcherDAOImplementation.getInstance().create(rnew);
		URI uri = new URI("/CRISSERVICE/rest/Researchers/" + r.getId());//la respuesta se devuelve por convencion a traves de la URI que permite acceder al objeto en cuestion 
		return Response.created(uri).build();
	}
	
	//los metodos REST pueden devolver un objeto o una respuesta (Response)
	
	@GET
	@Path("{id}/Publications") //cuando llegue el GET a traves de la ruta /Researchers/{id}/Publications
	@Produces(MediaType.APPLICATION_JSON)
	public List<Publication> readAllPublications(@PathParam("id") String id){
		//para coger el id de la ruta, ponemos PathParam("id")
		return PublicationDAOImplementation.getInstance().readAllPublications(id);
	}
	
	@GET
	@Path("{id}") //el path es /Researchers/[id]
	@Produces(MediaType.APPLICATION_JSON)
	public Response read(@PathParam("id") String id) {
		Researcher r = ResearcherDAOImplementation.getInstance().read(id);
		if(r==null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.ok(r, MediaType.APPLICATION_JSON).build();
		//la respuesta es el objeto que queriamos leer pero con formato JSON
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") String id, Researcher r) {
		Researcher rold = ResearcherDAOImplementation.getInstance().read(id);
		if ((rold==null) || (! rold.getId().contentEquals(r.getId())))
			return Response.notModified().build();
		ResearcherDAOImplementation.getInstance().update(r); //solo actualizamos si el objeto ya existe y su id de verdad coincide con el del objeto
		return Response.ok().build();
	}
	
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") String id) {
		Researcher rold = ResearcherDAOImplementation.getInstance().read(id);
		if(rold == null)
			return Response.notModified().build();
		ResearcherDAOImplementation.getInstance().delete(rold);
		return Response.ok().build();
	}
	
	@GET
	@Path("/email")
	@Produces(MediaType.APPLICATION_JSON)
	public Response readByEmail(@QueryParam("email") String email) {
		Researcher r = ResearcherDAOImplementation.getInstance().readByEmail(email);
		if (r == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.ok(r, MediaType.APPLICATION_JSON).build();
	}
	
	//TODO
	//Falta por implementar el metodo correspodniente al REST /Researchers/{id}/UpdatePublications
	//Ha dicho que lo hagamos al final porque puede fastidiarlo todo... (se hace con lo de Google)
	
	
	
}
