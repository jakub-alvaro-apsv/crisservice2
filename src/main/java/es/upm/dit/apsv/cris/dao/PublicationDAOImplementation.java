package es.upm.dit.apsv.cris.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Publication;

public class PublicationDAOImplementation implements PublicationDAO {
	
	private static PublicationDAOImplementation instance = null;
	
	private PublicationDAOImplementation() {}
	//truco para que solo haya una instancia de este objeto en este programa
	public static PublicationDAOImplementation getInstance() {
		if(null==instance) //si aun no se ha creado, se crea la primera vez
			instance = new PublicationDAOImplementation();
		return instance;
	}
	
	@Override
	public Publication create(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.save(publication);
			session.getTransaction().commit(); //todo ha ido bien
		}catch(Exception e){
		} finally {
			session.close(); //por si se produce un error en la sesion con la bbdd, la cerramos al recoger el error
		}
		return publication;
	}

	@Override
	public Publication read(String publicationId) {
		Session session = SessionFactoryService.get().openSession();
		Publication publication = null;
		try {
			session.beginTransaction();
			publication = session.get(Publication.class, publicationId);
			session.getTransaction().commit(); //todo ha ido bien
		}catch(Exception e){
		} finally {
			session.close(); //por si se produce un error en la sesion con la bbdd, la cerramos al recoger el error
		}
		return publication;
	}

	@Override
	public Publication update(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(publication);
			session.getTransaction().commit(); //todo ha ido bien
		}catch(Exception e){
		} finally {
			session.close(); //por si se produce un error en la sesion con la bbdd, la cerramos al recoger el error
		}
		return publication;
	}

	@Override
	public Publication delete(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.delete(publication);
			session.getTransaction().commit(); //todo ha ido bien
		}catch(Exception e){
		} finally {
			session.close(); //por si se produce un error en la sesion con la bbdd, la cerramos al recoger el error
		}
		return publication;
	}

	@Override
	public List<Publication> readAll() {
		Session session = SessionFactoryService.get().openSession();
		List<Publication> list = null;
		try {
			session.beginTransaction();
			list = (List<Publication>) session.createQuery("from Publication").getResultList();
			session.getTransaction().commit(); //todo ha ido bien
		}catch(Exception e){
		} finally {
			session.close(); 
		}
		return list;
	}

	@Override
	public List<Publication> readAllPublications(String researcherId) {
		//devuelve todas las publicaciones en las que el campo authors contiene a este investigador
		//podemos hacerla haciendo una consulta SQL o filtrando la lista de readAll()
		
				List<Publication> lista = new ArrayList<Publication>();
				for (Publication publication: this.readAll())
					if (publication.getAuthors().indexOf(researcherId)>-1)//indexOf devuelve -1 cuando el string no se encuentra en la cadena
						 lista.add(publication);
				return lista;
	}

}
